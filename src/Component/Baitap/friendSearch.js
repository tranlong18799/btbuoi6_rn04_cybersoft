import groudon from './images/groudon.png';
import kyorge from './images/kyorge.jpg';
import marshadow from './images/marshadow.jpg';
import volcarona from './images/volcarona.jpg';
import gyarados from './images/gyarados.jpg';
import kingdra from './images/kingdra.jpg';
import lapras from './images/lapras.jpg';
import milotic from './images/milotic.jpg';
import primarina from './images/primarina.jpg';
import tapufini from './images/tapufini.jpg';
import rayquaza from './images/rayquaza.jpg';

export const friendSearchList =[
    {
        id:'rayquaza-key',
        name:'Rayquaza',
        image:rayquaza
    },
    {
        id:'groudon-key',
        name:'Groudon',
        image:groudon
    },
    {
        id:'kyorge-key',
        name:'Kyorge',
        image:kyorge
    },
    {
        id:'volcarona-key',
        name:'Volcarona',
        image:volcarona
    },
    {
        id:'marshadow-key',
        name:'Marshadow',
        image:marshadow
    }
]

export const recomendFriend = [
    {
        id:'gyarados-key',
        name:'Gyarados',
        mutualF:15,
        image:gyarados
    },
    {
        id:'kingdra-key',
        name:'Kingdra',
        mutualF:16,
        image:kingdra
    },
    {
        id:'lapras-key',
        name:'Lapras',
        mutualF:17,
        image:lapras
    },
    {
        id:'milotic-key',
        name:'Milotic',
        mutualF:99,
        image:milotic
    },
    {
        id:'primarina-key',
        name:'Primarina',
        mutualF:90,
        image:primarina
    },
    {
        id:'tapufini-key',
        name:'Tapufini',
        mutualF:80,
        image:tapufini
    }
]