import { Image, Text, TouchableOpacity, View } from 'react-native';
import React, { Component } from 'react';
import { styles } from './style';
import { recomendFriend } from './friendSearch';

const { backgroundGrayLight, flexRow, roudedXl,
    w25Per, w50Per, w100Per, slImg, roundedFull, textXs, textBlack, fontBold, p3, mb5,
    btn, btnGray, justifyCenter, itemsCenter
} = styles;
const cirleImage = { ...slImg, ...roundedFull };
const txtXsBold = { ...fontBold, ...textBlack, ...textXs };
const cardStyle = { ...backgroundGrayLight, ...w100Per, ...flexRow, ...p3, ...mb5, ...roudedXl };
export default class CardFriendList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            addFriendList: []
        }
    }

    handlePress = (id) => {
        let listFriendTemp = [...this.state.addFriendList];
        let pokemon = recomendFriend.find(e => e.id === id);
        if (listFriendTemp.length === 0) {
            listFriendTemp.push(pokemon);
        } else {
            let index = listFriendTemp.findIndex(e => e.id === id);
            if (index === -1) {
                listFriendTemp.push(pokemon);
            } else {
                listFriendTemp.splice(index, 1);
            }
        }
        this.setState({ ...this.state, addFriendList: listFriendTemp });
    }

    render() {
        const { item: { name, image, mutualF, id } } = this.props;
        const { addFriendList } = this.state;
        return (
            <View style={cardStyle}>
                <View style={w25Per}>
                    <Image source={image} style={cirleImage} />
                </View>
                <View style={[w50Per, justifyCenter]}>
                    <Text style={txtXsBold}>{name}</Text>
                    <Text>{mutualF} bạn chung</Text>
                </View>
                <View style={[w25Per, justifyCenter, itemsCenter]}>
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => this.handlePress(id)}
                    >
                        {addFriendList?.find(e => e.id === id) ?
                            <Text style={[btn, btnGray]}>Hủy bỏ</Text> :
                            <Text style={btn}>Kết bạn</Text>}
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
