import { FlatList, Image, Text, View, ScrollView } from 'react-native';
import React, { Component } from 'react';
import { styles } from './style';
import { friendSearchList, recomendFriend } from './friendSearch';
import CardFriendList from './CardFriendList';
import Icon from 'react-native-vector-icons/Entypo';


const { container, flexRow, justifyStart, Grow0, slImg,
    roundedFull, m4, fontBold, textLg, textSm, textBlack, h20Per, h80Per, p2
} = styles;
const flexRowStart = { ...flexRow, ...justifyStart, ...Grow0 };
const circleImage = { ...slImg, ...roundedFull };
const txtLgBold = { ...fontBold, ...textLg, ...textBlack,...p2 };
const txtSmBold = { ...fontBold, ...textSm, ...textBlack };
const space20Per = { ...h20Per, ...p2 };
const space80Per = { ...h80Per, ...p2 };
export default class FriendComponent extends Component {
    constructor(props){
        super(props);
    }

    renderFriendSearch = ({ item, index }) => {
        return <View style={m4}>
            <Image source={item.image} style={circleImage} />
            <Text style={txtSmBold}>{item.name}</Text>
        </View>
    }
 

    renderFriendRecomend = ({ item, index }) => {
        return <CardFriendList item={item} />
    }

    render() {
        return (
            <View style={container}>
                <View style={space20Per}>
                    <Text style={txtLgBold}><Icon name="back-in-time" size={18} color="black" /> Danh sách tìm kiếm gần đây</Text>
                    <FlatList
                        renderItem={this.renderFriendSearch}
                        data={friendSearchList}
                        contentContainerStyle={flexRowStart}
                        horizontal={true}
                    />
                </View>
                <View style={space80Per}>
                    <Text style={txtLgBold}><Icon name="users" size={18} color="black" /> Gợi ý kết bạn</Text>
                    <View style={{ padding: 5 }}>
                        <FlatList
                            renderItem={this.renderFriendRecomend}
                            data={recomendFriend}
                        // contentContainerStyle={flexRowStart}
                        />
                    </View>
                </View>
            </View>
        );
    }
}
